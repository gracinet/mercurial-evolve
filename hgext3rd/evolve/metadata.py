# define Mercurial extension metadata for evolution
#
# Copyright 2017 Pierre-Yves David <pierre-yves.david@ens-lyon.org>
#
# This software may be used and distributed according to the terms of the
# GNU General Public License version 2 or any later version.

__version__ = b'10.5.0'
testedwith = b'4.8 4.9 5.0 5.1 5.2 5.3 5.4 5.5 5.6 5.7 5.8 5.9 6.0 6.1'
minimumhgversion = b'4.8'
buglink = b'https://bz.mercurial-scm.org/'
