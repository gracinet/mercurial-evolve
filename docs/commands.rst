-------------------------
Evolve Commands Reference
-------------------------

.. highlight:: none

amend
-----

.. hghelp:: amend

evolve
------

.. hghelp:: evolve

fold
----

.. hghelp:: fold

metaedit
--------

.. hghelp:: metaedit

next
----

.. hghelp:: next

obslog
------

.. hghelp:: obslog

pdiff
-----

.. hghelp:: pdiff

previous
--------

.. hghelp:: previous

prune
-----

.. hghelp:: prune

pstatus
-------

.. hghelp:: pstatus

split
-----

.. hghelp:: split

touch
-----

.. hghelp:: touch

uncommit
--------

.. hghelp:: uncommit
